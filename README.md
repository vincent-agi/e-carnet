----------English version----------
Hi,
I present you the computer application E-Carnet. This application works on Windows (7 and higher), macOS and linux.
E-Carnet is an application providing you with the functionalities to manage repairs on your personal vehicles. One of its main features is to be able to generate a relatively complete report on the repair history of your vehicle based on the repairs recorded.
To run E-Carnet you need a minimum of 4 gigabytes of ram on a windows PC, 2 gigabytes on linux and mac. Your processor architecture must be x64 (x86 is not tested).
Normally executables are available in the "release-builds" folder. Then select the folder of your operating system. Then run the "e-carnet" binary.
Under Windows simply double click on it. Under Linux and Mac launch a terminal and type the following command "./e-carnet".
If you wish to compile the code yourself because you have made changes to it, for example, launch a terminal and do "npm install", then "npm run package-win" to compile a windows binary, "npm run package-linux" for a linux binary or "npm run package-mac" for a Mac binary.
Good use,

Vincent

----------version française----------
Bonjour à tous,
Je vous présente l'application pour ordinateur E-Carnet. Cette application fonction sur Windows (7 et supérieur), macOS et linux.
E-Carnet est une application vous fournissant les fonctionnalités pour gérer les réparations sur vos véhicules personnels. Une de ses fonctionnalité principale est de pouvoir générer un rapport relativement complet sur l'historique des réparations de votre véhicule sur la base des celles enregistrées.
Pour lancer E-Carnet vous devez disposer au minimum de 4 giga-octet de ram sur un pc windows, 2 giga-octet sur un linux et mac. Votre architecture processeur doit être en x64 (x86 n'est pas testé).
Normalement les exécutables sont disponibles dans le dossier "release-builds". Sélectionnez ensuite le dossier de votre système d'exploitation. Exécuter ensuite le le binaire "e-carnet".
Sous Windows double cliquez simplement dessus. Sous Linux et mac lancer un terminal et tapez la commande suivante "./e-carnet".
Si vous souhaitez compiler vous même le code car vous y avez apporté des modifications par exemple, lancer un terminal et faites "npm install", puis "npm run package-win" pour compiler un binaire windows, "npm run package-linux" pour un binaire linux ou "npm run package-mac" pour un binaire Mac.
Bonne utilisation,

Vincent
