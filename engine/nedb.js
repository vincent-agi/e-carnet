const Datastore = require('nedb');
const app = require('electron').remote.app
let repairs = new Datastore({ filename:app.getPath('home')+'\/.e-carnet\/repairs.ecarnet',autoload: true});
let car = new Datastore({ filename:app.getPath('home')+'\/.e-carnet\/car.ecarnet',autoload: true});
let preference = new Datastore({ filename:app.getPath('home')+'\/.e-carnet\/preference.ecarnet',autoload: true});
let _id = null;
let _class_current_language = null;
class CarNetManagement {

    constructor(callback){
        preference.find({},(err,docs)=>{
            if(docs.length >0){
                _class_current_language = docs[0].lang
                callback({lang:docs[0].lang})
            }else{
                preference.insert({lang:'en'},(error,doc)=>{
                    callback({lang:'en'})
                })
            }
        })
    }


    UpdateLanguage(language,callback){
        console.log({lang:_class_current_language})
        preference.update({lang:_class_current_language}, { $set:{lang:language}},{ multi: true },(err,num)=>{
            callback(num)
        })



    }

    CarExist(callback){
        car.find({}, function (err, docs) {
           if(docs.length>0){
            _id = docs[0]._id;
           }

            callback(docs)
          });
    }

    AddCar(registration,serial_number,fuel,type,circulation,brand,model,callback){
        let doc_car = {
            registration:registration,
            serial_number:serial_number,
            fuel:fuel,
            type:type,
            circulation:circulation,
            brand:brand,
            model:model,
            date:new Date()
        }

        car.insert(doc_car,(error,doc)=>{
            _id = doc._id;
            callback(true)
        })
    }

        AddRepairRecord(title,status,date_create,date_completed,comment,millage,callback){
            let doc_car = {
                title:title,
                status:status,
                date_create:date_create,
                date_completed:date_completed,
                comment:comment,
                date:new Date(),
                millage:millage,
                car_id:_id,
            }
            repairs.insert(doc_car,(error,doc)=>{
                callback(true)
            })

        }

        AddBackUpRepair(title,status,date_create,date_completed,comment,date,millage,car_id,callback){
            let doc_car = {
                title:title,
                status:status,
                date_create:date_create,
                date_completed:date_completed,
                comment:comment,
                date:date,
                millage:millage,
                car_id:car_id,
            }
            repairs.insert(doc_car,(error,doc)=>{
                callback(true)
            })
        }


        GetAllRepairs(callback){
            repairs.find({}, function (err, docs) {
                callback(docs)
              });
        }

        UpdateRepairState(_id,state,date,repair,callback){

            repairs.update({_id:_id}, { $set:{date_completed:date,status:state,comment:repair}},{ multi: true },(err,num)=>{
                console.log(num);
                callback(num)
            })



        }

        UpdateTitle(_id,new_title,callback){

            repairs.update({_id:_id}, { $set:{title:new_title}},{ multi: true },(err,num)=>{
                callback(num)
            })



        }

        GetInfoOnOne(_id,callback){

            repairs.find({_id:_id},(err,record)=>{
                callback(record)
            })



        }

        DeleteRepair(_id,callback){
            repairs.remove({ _id:_id }, {}, function (err, numRemoved) {
                callback(numRemoved)
              });
        }

        UpdateVehicleInformation(_id,registration,serial_number,fuel,type,circulation,brand,model,callback){
            car.update({_id:_id},{ $set:{
            registration:registration,
            serial_number:serial_number,
            fuel:fuel,
            type:type,
            circulation:circulation,
            brand:brand,
            model:model,
            }},{ multi: true },(err,number)=>{
                callback({state:true,number:number})
            })
        }

        DropAllDocument(callback){
            repairs.remove({}, { multi: true }, function (err, numRemoved) {
                car.remove({}, { multi: true }, function (err, numRemoved) {
                    callback(numRemoved);
                });
            });
        }
}


