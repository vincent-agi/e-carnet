const { app, BrowserWindow,Tray,ipcMain,Menu} = require('electron')
const path = require('path');
const win = null;
const { dialog } = require('electron')
const print_window =null;
global.share = { app, BrowserWindow,print_window, ipcMain,Tray,Menu,win};
process.env["NODE_OPTIONS"] = '--no-force-async-hooks-checks';
function createWindow () {
  global.share.win = new BrowserWindow({
    width: 1200,
    height: 750,
    minWidth:1200,
    minHeight: 750,
    webPreferences: {
        nodeIntegration: true,
        devTools:true,
        enableRemoteModule: true
      },
  })

  global.share.win.loadFile('./pages/index.html')
  global.share.win.setIcon(path.join(__dirname, '/assets/images/logo.png'));
  global.share.win.setMenuBarVisibility(true);

}

app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()

  }
})

global.share.ipcMain.on("PRINT:RECORDS",(err,reg)=>{
  global.share.print_window = new BrowserWindow({
    title: "Follow Car Service",
    show: false,
    width: 1300,
    height: 1000,
    resizable:true,
    visibility:true,
    maximizable:true,
    webPreferences: {
      nodeIntegration: true,
      devTools:false,
      enableRemoteModule: true
    },
  });

  global.share.print_window.loadURL(`file://${__dirname}/pages/print.html`);
  global.share.print_window.once("ready-to-show", () => {
    // global.share.print_window.show();
    // global.share.print_window.setMenuBarVisibility(true);
  });
})

ipcMain.on("OPEN:LOCATION",(err,res)=>{
  dialog.showOpenDialog(global.share.win,{ properties: ['openDirectory'] }).then((resolve)=>{
    global.share.win.webContents.send(res.callback,resolve);
  })
})

ipcMain.on("OPEN:FILE",(err,res)=>{
  let options = {
    // See place holder 1 in above image
    title : "Please select Backup File",

    // See place holder 3 in above image
    buttonLabel : "Select EcarnetFile",

    // See place holder 4 in above image
    filters :[
     {name: '.ecarnet files', extensions: ['ecarnet']},
    ],
    properties: ['openFile']
   }

  dialog.showOpenDialog(global.share.win,options).then((resolve)=>{
    global.share.win.webContents.send(res.callback,resolve);
  })
})

